/*
Attendance code sending system for UCL Physics & Astronomy
Copyright (C) 2018  Balázs Dura-Kovács

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.durastudio.attendance

import android.annotation.TargetApi
import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.webkit.*
import android.webkit.WebView
import android.webkit.WebViewClient
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.durastudio.attendance.R.id.webview
import java.util.*
import org.json.JSONObject
import android.preference.PreferenceManager
import android.content.SharedPreferences
import android.net.http.SslCertificate
import android.net.http.SslError
import android.view.*
import java.io.BufferedInputStream
import java.io.InputStream
import java.lang.Exception
import java.lang.reflect.Field
import java.net.URL
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager


class LoginActivity : AppCompatActivity() {

    val token = BuildConfig.UCLAPI_TOKEN
    val clientId = BuildConfig.UCLAPI_CLIENT_ID
    val clientSecret = BuildConfig.UCLAPI_CLIENT_SECRET
    val callbackUrl =  BuildConfig.UCLAPI_CALLBACK_URL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val loginButton = findViewById<Button>(R.id.login_button)

        loginButton.setOnClickListener{startLogin()}
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.loginmenu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.upgrades -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/balping/attendance/-/releases"))
                startActivity(browserIntent)
                true
            }
            R.id.gitlab -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/balping/attendance"))
                startActivity(browserIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun startLogin() {
        val webview = findViewById<WebView>(R.id.webview)

        webview.getSettings().setJavaScriptEnabled(true)


        webview.loadUrl("https://uclapi.com/oauth/authorise?client_id=" + this.clientId + "&state=" + UUID.randomUUID().toString().replace("-", ""))
        webview.visibility = View.VISIBLE

        val callbackUrl = this.callbackUrl

        webview.webViewClient = object : WebViewClient() {

            override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
                val url : String = request?.url.toString()
                if(url == "https://uclapi-static.s3.amazonaws.com/static/js/authorise.js"){
                    return WebResourceResponse("application/javascript", "UTF-8", getAssets().open("authorise.js"))
                }


                if(url.startsWith("http://")){
                    try {

                        val httpsUrl = URL(url.replace("http://", "https://"))
                        val connection = httpsUrl.openConnection()
                        return WebResourceResponse(connection.getContentType(), connection.getContentEncoding(), connection.getInputStream())
                    }catch (e: Exception){}
                }

                return super.shouldInterceptRequest(view, request)
            }

            override fun onPageFinished(view: WebView, url: String) {
                if(url.startsWith(callbackUrl)) {

                    val parsed = Uri.parse(url);
                    val code = parsed.getQueryParameter("code")
                    val result = parsed.getQueryParameter("result")

                    webview.visibility = View.GONE
                    webview.loadUrl("about:blank")

                    if(result == "allowed" && code.isNotEmpty()){
                        finishLogin(code)
                    } else {
                        Toast.makeText(this@LoginActivity, "Login failed. Try again.", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            // https://developer.android.com/training/articles/security-ssl.html#UnknownCa
            override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                //super.onReceivedSslError(view, handler, error)
                val cf: CertificateFactory = CertificateFactory.getInstance("X.509")
                val caInput: InputStream = BufferedInputStream(
                    getAssets()
                    .openFd("quovadis_qvsslg3_der.crt")
                    .createInputStream()
                )
                val ca: X509Certificate = caInput.use {
                    cf.generateCertificate(it) as X509Certificate
                }

                val keyStoreType = KeyStore.getDefaultType()
                val keyStore = KeyStore.getInstance(keyStoreType).apply {
                    load(null, null)
                    setCertificateEntry("ca", ca)
                }

                val tmfAlgorithm: String = TrustManagerFactory.getDefaultAlgorithm()
                val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm).apply {
                    init(keyStore)
                }

                var pass = false

                val cert : SslCertificate? = error?.certificate
                val f : Field = cert?.javaClass!!.getDeclaredField("mX509Certificate")
                f.setAccessible(true)
                val x509 : X509Certificate = f.get(cert) as X509Certificate

                val chain  = arrayOf(x509)



                for(tm in tmf.trustManagers){
                    if(tm is X509TrustManager) {
                        try{
                            tm.checkServerTrusted(chain, "generic")
                            pass = true
                            break
                        } catch (e: Exception){
                            pass = false
                        }

                    }
                }

                if(pass){
                    handler?.proceed()
                }else{
                    super.onReceivedSslError(view, handler, error)
                }
            }
        }

    }

    fun finishLogin(code: String){
        val queue = Volley.newRequestQueue(this)
        val url = "https://uclapi.com/oauth/token?code=" + code + "&client_id=" + this.clientId + "&client_secret=" + this.clientSecret

        val stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener<String> { response ->
                    val jsonObject = JSONObject(response)
                    val token = jsonObject.getString("token")

                    val preferences = PreferenceManager.getDefaultSharedPreferences(this)
                    val editor = preferences.edit()
                    editor.putString("token", token)
                    editor.apply()

                    storeUserId(token)

                },
                Response.ErrorListener { Toast.makeText(this@LoginActivity, "Login failed. Try again.", Toast.LENGTH_SHORT).show() }
        )

        queue.add(stringRequest)
    }

    fun storeUserId(usertoken: String){
        val queue = Volley.newRequestQueue(this)
        val url = "https://uclapi.com/oauth/user/studentnumber?token=" + usertoken + "&client_secret=" + this.clientSecret

        val stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener<String> { response ->
                    val jsonObject = JSONObject(response)
                    val studentNumber = jsonObject.getString("student_number")

                    val preferences = PreferenceManager.getDefaultSharedPreferences(this)
                    val editor = preferences.edit()
                    editor.putInt("studentnumber",  studentNumber.toInt())
                    editor.apply()

                    val intent = Intent(this, SubmitActivity::class.java)
                    startActivity(intent)

                    finish()

                },
                Response.ErrorListener { response -> Toast.makeText(this@LoginActivity, "Error: " + response.message, Toast.LENGTH_SHORT).show() }
        )

        queue.add(stringRequest)
    }
}
