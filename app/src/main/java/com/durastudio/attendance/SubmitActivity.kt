/*
Attendance code sending system for UCL Physics & Astronomy
Copyright (C) 2018  Balázs Dura-Kovács

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package com.durastudio.attendance

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import android.widget.AdapterView.OnItemClickListener
import com.android.volley.ClientError
import com.g00fy2.versioncompare.Version
import org.json.JSONArray
import java.io.File


class SubmitActivity : AppCompatActivity() {

    lateinit var codeFirstPartView : TextView
    lateinit var preferences : SharedPreferences

    var codeFirstPart = ""
        set(value) {
            this.codeFirstPartView.text = value
            field = value
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.refresh -> {
                val file = File(cacheDir, "timetable.json")
                file.delete()
                loadTimetable()
                true
            }
            R.id.logout -> {
                logout()
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
                true
            }
            R.id.log -> {
                val intent = Intent(this, LogActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.upgrades -> {
                checkForUpdates(true)
                true
            }
            R.id.gitlab -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/balping/attendance"))
                startActivity(browserIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submit)

        this.codeFirstPartView = this.findViewById<TextView>(R.id.code_first_part)
        this.preferences = PreferenceManager.getDefaultSharedPreferences(this)

        val submitButton = findViewById<Button>(R.id.submit)
        submitButton.setOnClickListener{submit()}

        loadTimetable()

        Handler().postDelayed({
            checkForUpdates()
        }, 6000)
    }

    fun loadTimetable(){
        var timetable = ""
        try {
            val file = File(cacheDir, "timetable.json")
            timetable = file.readText()
        }catch (e: Exception){

        }


        if(timetable.isNotEmpty()){
            showTimetable(timetable)
            return
        }


        val queue = Volley.newRequestQueue(this)

        val url = "https://uclapi.com/timetable/personal?token=" + this.preferences.getString("token", "") + "&client_secret=" + BuildConfig.UCLAPI_CLIENT_SECRET

        val stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener<String> { response ->
                    val file = File(cacheDir, "timetable.json")
                    file.writeText(response)
                    showTimetable(response)
                },
                Response.ErrorListener {
                    if(it is ClientError && it.networkResponse.statusCode == 400){
                        val intent = Intent(this, LoginActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(this@SubmitActivity, "Failed to fetch timetable: \n" + it.localizedMessage, Toast.LENGTH_LONG).show()
                        noLectures()
                    }
                }
        )

        queue.add(stringRequest)
    }

    private fun showTimetable(rawJson: String?) {
        val jsonObject = JSONObject(rawJson)

        val timetable = jsonObject.getJSONObject("timetable")
        val now = Date()
        val isoDate = SimpleDateFormat("yyyy-MM-dd", Locale.UK)
        val isoTime = SimpleDateFormat("HH:mm", Locale.UK)
        isoDate.timeZone = TimeZone.getTimeZone("Europe/London")
        isoTime.timeZone = TimeZone.getTimeZone("Europe/London")

        val todayString = isoDate.format(now)
        //val todayString = "2019-11-12" // TODO: REMOVE
        val nowTime = isoTime.format(now)

        try {
            val today = timetable.getJSONArray(todayString)

            val lectures = ArrayList<JSONObject>()
            for (i in 0 until today.length()) {
                lectures.add(today.getJSONObject(i))
            }

            lectures.sortBy { it.getString("start_time") }

            var latestIndex = lectures.indexOfLast {
                it.getString("start_time") < nowTime
            }

            if (latestIndex < 0) {
                latestIndex = 0
            }

            val choosableLectires = ArrayList<JSONObject>()

            if (latestIndex > 0) {
                choosableLectires.add(lectures.get(latestIndex - 1))
            }

            choosableLectires.add(lectures.get(latestIndex))

            if (latestIndex < lectures.size - 1) {
                choosableLectires.add(lectures.get(latestIndex + 1))
            }

            showLectureList(choosableLectires)


        } catch (e: Exception) {
            noLectures()
        }
    }

    fun showLectureList(lectures: ArrayList<JSONObject>){
        val list = findViewById<ListView>(R.id.lecture_list)
        list.visibility = View.VISIBLE
        findViewById<TextView>(R.id.no_lectures).visibility = View.GONE

        val text = lectures.map {
            val module = it.getJSONObject("module")
            it.getString("start_time") + " – " +
                    it.getString("end_time") + "  " +
                    module.getString("name") + " " + module.getString("module_id")
        }

        val adapter = ArrayAdapter<String>(this,
                R.layout.lecture_list_item, text)

        list.adapter = adapter

        list.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val studentNumber = this.preferences.getInt("studentnumber", 0)
            var courseCode = lectures.get(position).getJSONObject("module").getString("module_id")
            courseCode = """\d+$""".toRegex().find(courseCode)!!.value

            // High performance computing default code
            if(courseCode == "0102") {
                val codeInput: EditText = findViewById<EditText>(R.id.codeText)
                codeInput.setText("0000")
            }

            this.codeFirstPart = "uclpa " + studentNumber.toString() + "," + courseCode + ","
        }


    }

    fun noLectures(){
        findViewById<TextView>(R.id.no_lectures).visibility = View.VISIBLE
        findViewById<ListView>(R.id.lecture_list).visibility = View.GONE
    }

    fun submit(){
        val codeInput: EditText = findViewById<EditText>(R.id.codeText)
        val code = codeInput.text.toString()

        if(code.isEmpty() || this.codeFirstPart.isEmpty()){
            return
        }

        val fullCode = this.codeFirstPart + code
        //val fullCode = "fire fg" //TODO: remove

        val url = "https://www.textwall.co.uk/post/validate"

        val queue = Volley.newRequestQueue(this)
        val stringRequest = object : StringRequest(Request.Method.POST, url,
                Response.Listener<String> { response ->
                    if(! response.contains("The code you have used is invalid.")){
                        this.log(fullCode)

                        Toast.makeText(this@SubmitActivity, "Code successfully posted", Toast.LENGTH_LONG).show()
                        codeInput.text.clear()
                    }else{
                        Toast.makeText(this@SubmitActivity, "Posting code failed", Toast.LENGTH_LONG).show()
                    }

                },
                Response.ErrorListener { Toast.makeText(this@SubmitActivity, "Posting code failed:\n" + it.localizedMessage, Toast.LENGTH_LONG).show() }
        ) {
            override fun getParams(): Map<String, String> {
                val params= HashMap<String, String>();
                params["posttoweb"] = fullCode

                return params
            }
        }




        queue.add(stringRequest)


    }

    fun log(fullCode: String){
        val now = Date()
        val iso = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK)
        iso.timeZone = TimeZone.getTimeZone("Europe/London")

        val nowTime = iso.format(now)
        val cont = "$nowTime $fullCode\n"

        // save to log
        openFileOutput("log.txt", Context.MODE_PRIVATE or Context.MODE_APPEND).use {
            it.write(cont.toByteArray())
        }

    }

    fun logout(){
        val file = File(cacheDir, "timetable.json")
        file.delete()
        val editor = this.preferences.edit()
        editor.clear()
        editor.apply()
    }

    fun checkForUpdates(forceUpdate: Boolean = false){
        val lastChecked = preferences.getLong("upgrade_last_checked", 0)
        val now = System.currentTimeMillis()

        // check every 2 days only
        if(!forceUpdate && now - lastChecked < 2 * 24 * 60 * 60 * 1000){
            return
        }


        val url = "https://gitlab.com/api/v4/projects/balping%2Fattendance/repository/tags?per_page=1"
        val queue = Volley.newRequestQueue(this)

        val stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener<String> { response ->
                    try{
                        val tags = JSONArray(response)
                        val tag = tags.get(0) as JSONObject
                        val new = tag.getJSONObject("release").getString("tag_name").substring(1)
                        val current = BuildConfig.VERSION_NAME
                        if(Version(new).isHigherThan(current)){
                            val context = (getApplicationContext() as MyApplication).activeActivity
                            val dialogBuilder = AlertDialog.Builder(context)

                            dialogBuilder
                                    .setMessage("A new version of this app has been released. Would you like to download it now?")
                                    .setCancelable(false)
                                    .setPositiveButton("Download") { dialog, id ->
                                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/balping/attendance/releases"))
                                        startActivity(browserIntent)
                                    }
                                    .setNegativeButton("Cancel") { dialog, id -> dialog.cancel() }

                            val alert = dialogBuilder.create()
                            alert.setTitle("Upgrade to " + new)
                            alert.show()
                        }else if(forceUpdate){
                            val context = (getApplicationContext() as MyApplication).activeActivity
                            val dialogBuilder = AlertDialog.Builder(context)

                            dialogBuilder
                                    .setMessage("There aren't any new versions available.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK") { dialog, id -> dialog.cancel() }

                            val alert = dialogBuilder.create()
                            alert.show()
                        }

                        val editor = preferences.edit()
                        editor.putLong("upgrade_last_checked", now)
                        editor.apply()

                    }catch (e: Exception){

                    }

                },
                Response.ErrorListener { }
        )

        queue.add(stringRequest)

    }


}
