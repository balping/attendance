package com.durastudio.attendance

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import java.io.File

class LogActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log)

        val logTextView = this.findViewById<TextView>(R.id.logText)
        try {
            val file = File(filesDir, "log.txt")
            val log = file.readText()
            logTextView.text = log
        } catch (e: Exception){

        }

    }
}
