# UCL Physics Attendance

Little app that helps sending attendance codes.

## Install

This app is compatible with Android 5.0 and newer.

You can download the latest release from [this page](https://gitlab.com/balping/attendance/releases).

At some stage you'll need to enable app installs from unknown sources. Depending on your Android version,
you might be prompted to do so, or you might need to find it under Settings.

## Screenshot

<img src="https://i.imgur.com/3o3gszY.png" alt="screenshot" width="270px">

## Building

### Requirements:
- Android SDK 28
- Gradle 4.6
- Android Studio (optional)

### UCL API

Obtain keys from [UCL API](https://uclapi.com/dashboard/)

```
cd app
cp api.gradle.example api.gradle
```

Add API keys to `api.gradle`

### Build

```
./gradlew :app:installDebug
```



## License

This piece of software is distributed under GPLv3.

## Changelog

### v0.1.6 2019-11-04

#### Added
- High Performance Computing default code

### v0.1.5 2019-10-07

#### Fixed
- Certificate issues when logging in

#### Added
- Link to gitlab on login screen

### v0.1.4 2019-01-21

#### Fixed
- No log off when viewing the log

### v0.1.3 2019-01-19

#### Added
- Log submitted codes

### v0.1.2 2018-10-10

#### Added
- Android 5.0 support

#### Changed
- UI improvements

### v0.1.1 2018-10-06

#### Added
- Timetable caching
- Option to logout
- Automatic checks for upgrades

#### Changed
- UI improvements
- better error handling


### v0.1.0 2018-10-05
- Initial release